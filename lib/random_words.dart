import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:flutter/services.dart';

class RandomWordList extends StatefulWidget {
  @override
  RandomWordState createState() => RandomWordState();
}

class RandomWordState extends State<RandomWordList> {
  final _randomWords = <WordPair>[];
  final _savedWordPairs = Set<WordPair>();

  Widget _buildList() {
    return ListView.builder(
      padding: const EdgeInsets.all(15),
      itemBuilder: (context, item) {
        if (item.isOdd)
          return Divider(
            thickness: 1,
          );

        final index = item ~/ 2;

        if (index >= _randomWords.length) {
          _randomWords.addAll(generateWordPairs().take(10));
        }

        return _buildRow(_randomWords[index]);
      },
    );
  }

  Widget _buildRow(WordPair pair) {
    final alreadySaved = _savedWordPairs.contains(pair);

    void _toggleSaved() => {
          setState(() {
            if (alreadySaved) {
              _savedWordPairs.remove(pair);
            } else {
              _savedWordPairs.add(pair);
            }
          })
        };

    return GestureDetector(
        onDoubleTap: _toggleSaved,
        child: ListTile(
          title: Text(
            pair.asPascalCase,
            style: TextStyle(
              fontSize: 17,
            ),
          ),
          trailing: IconButton(
            icon: Icon(
              alreadySaved ? Icons.favorite : Icons.favorite_border_rounded,
              color: alreadySaved ? Colors.red[400] : null,
            ),
            onPressed: _toggleSaved,
            tooltip: "Favourite",
          ),
        ));
  }

  void _pushSavedPage() => {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _savedWordPairs.map((WordPair pair) {
            return ListTile(
              contentPadding: const EdgeInsets.all(15),
              title: Text(
                pair.asPascalCase,
                style: TextStyle(fontSize: 17),
              ),
              trailing: IconButton(
                icon: Icon(Icons.copy),
                onPressed: () {
                  Clipboard.setData(
                      ClipboardData(text: "${pair.asPascalCase}"));
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text('Copied ${pair.asPascalCase} to clipboard!'),
                    duration: const Duration(seconds: 1),
                  ));
                },
              ),
            );
          });

          final List<Widget> divided =
              ListTile.divideTiles(tiles: tiles, context: context)
                  .toList(growable: true);

          return Scaffold(
            appBar: AppBar(
              title: Text('Favourite'),
            ),
            body: ListView(
              padding: const EdgeInsets.all(15),
              children: divided,
            ),
          );
        }))
      };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('WordPair Generator'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.list),
            tooltip: 'Favourite Words',
            onPressed: _pushSavedPage,
          )
        ],
      ),
      body: _buildList(),
    );
  }
}
